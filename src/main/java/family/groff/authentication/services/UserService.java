package family.groff.authentication.services;

import family.groff.authentication.interfaces.PasswordHashService;
import family.groff.authentication.interfaces.UsersApiDelegate;
import family.groff.authentication.models.User;
import family.groff.authentication.repositories.UserRepository;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

@Service
public class UserService implements UsersApiDelegate {
    UserRepository userRepository;
    PasswordHashService passwordHashService;

    UserService(UserRepository userRepository, PasswordHashService passwordHashService) {
        this.userRepository = userRepository;
        this.passwordHashService = passwordHashService;
    }

    @Override
    public ResponseEntity<HashMap<String, List<String>>> createUser(User user) {
        HashMap<String, List<String>> errors = null;
        if (!isValidEmail(user.getEmailId())) {
            errors = buildErrorList(null, "The email address is not valid");
        }
        if (user.getPassword().length() < 8) {
            errors = buildErrorList(errors, "The password entered is too short. Must be 8 characters or greater");
        } else if (user.getPassword().length() > 16) {
            errors = buildErrorList(errors, "The password entered is too long. Must be 16 characters or less");
        }

        if (!Pattern.compile("[0-9]").matcher(user.getPassword()).find()) {
            errors = buildErrorList(errors, "The password entered must contain a number");
        }
        if (!Pattern.compile("[A-Z]").matcher(user.getPassword()).find()) {
            errors = buildErrorList(errors, "The password entered must contain a capital");
        }
        if (!Pattern.compile("[^A-Za-z0-9]").matcher(user.getPassword()).find()) {
            errors = buildErrorList(errors, "The password entered must contain a special character");
        }

        if (userRepository.existsByEmailId(user.getEmailId())) {
            errors = buildErrorList(errors, "The email address is already in use");
            return new ResponseEntity<>(
                    errors,
                    HttpStatus.FORBIDDEN
            );
        }
        user.setPassword(passwordHashService.hashPassword(user.getPassword()));
        if (errors == null) {
            userRepository.save(user);
        }
        if (errors != null) {
            return new ResponseEntity<>(
                    errors,
                    HttpStatus.BAD_REQUEST
            );
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @Override
    public ResponseEntity<List<User>> listUsers(Integer limit) {
        return new ResponseEntity<List<User>>(
                StreamSupport.stream(userRepository.findAll().spliterator(), false).toList(),
                HttpStatus.OK
        );
    }

    @Override
    public ResponseEntity<User> showUserById(String userId) {
        try {
            return userRepository.findById(Long.parseLong(userId))
                    .map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                    .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
        }
        catch (java.lang.NumberFormatException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    private HashMap<String, List<String>> buildErrorList(String error) {
        var errors = new HashMap<String, List<String>>();
        errors.put("errors", new ArrayList<String>());
        return buildErrorList(errors, error);
    }

    private HashMap<String, List<String>> buildErrorList(HashMap<String, List<String>> errors, String error) {
        if (errors == null) {
            errors = new HashMap<String, List<String>>();
            errors.put("errors", new ArrayList<String>());
        }

        errors.get("errors").add(error);
        return errors;
    }

    private boolean isValidEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }
}
