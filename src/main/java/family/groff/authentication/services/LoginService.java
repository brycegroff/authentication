package family.groff.authentication.services;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.shaded.json.JSONObject;
import family.groff.authentication.interfaces.LoginApiDelegate;
import family.groff.authentication.interfaces.PasswordHashService;
import family.groff.authentication.models.User;
import family.groff.authentication.repositories.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.HashMap;

@Service
public class LoginService implements LoginApiDelegate {
    UserRepository userRepository;
    PasswordHashService passwordHashService;
    LoginService(UserRepository userRepository, PasswordHashService passwordHashService) {
        this.userRepository = userRepository;
        this.passwordHashService = passwordHashService;
    }

    @Override
    public ResponseEntity<String> login(String emailId, String password) {
        var hashedPassword = passwordHashService.hashPassword(password);
        var user = userRepository.findByEmailId(emailId);
        if (hashedPassword.equals(user.getPassword())) {
            try {
                JWSObject jwsObject = new JWSObject(
                        new JWSHeader(JWSAlgorithm.HS256),
                        new Payload(generatePaylaod(user))
                );

                byte[] sharedKey = new byte[32];
                new SecureRandom().nextBytes(sharedKey);
                jwsObject.sign(new MACSigner(sharedKey));

                return new ResponseEntity<>(jwsObject.serialize(), HttpStatus.OK);
            } catch (JOSEException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);

    }

    private String generatePaylaod(User user) {
        var payload = new HashMap<String, String>();
        payload.put("sub", user.getEmailId());
        payload.put("first_name", user.getFirstName());
        payload.put("last_name", user.getLastName());
        payload.put("age", user.getAge().toString());
        payload.put("phone_number", user.getPhone());
        payload.put("email", user.getEmailId());
        payload.put("email_verified", "true");
        return new JSONObject(payload).toJSONString();
    }
}
