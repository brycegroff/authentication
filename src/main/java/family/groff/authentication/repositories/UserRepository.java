package family.groff.authentication.repositories;

import family.groff.authentication.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    boolean existsByEmailId(String email);
    User findByEmailId(String email);
}
