package family.groff.authentication.interfaces;

import family.groff.authentication.api.ApiUtil;
import family.groff.authentication.api.OrdersApiController;
import family.groff.authentication.models.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

/**
 * A delegate to be called by the {@link OrdersApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-10-08T14:53:29.739041194-07:00[America/Los_Angeles]")

public interface OrdersApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /orders/{orderId}/approve : Approve a order
     *
     * @param orderId The id of the order to retrieve (required)
     * @return Approval response (status code 201)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see OrdersApi#approveOrders
     */
    default ResponseEntity<Boolean> approveOrders(String orderId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "false";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /orders : List all orders
     *
     * @param status status of the order REQUESTED,APPROVED,DENIED (optional)
     * @param reportee if true returns orders of reportees by name for approval (optional)
     * @param limit How many items to return at one time (max 100) (optional)
     * @return A paged array of orders (status code 200)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see OrdersApi#listOrders
     */
    default ResponseEntity<List<Order>> listOrders(String status,
                                                   String reportee,
                                                   Integer limit) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"reason\" : \"reason\", \"comment\" : \"comment\", \"id\" : 0, \"items\" : [ { \"price\" : 1, \"name\" : \"name\", \"description\" : \"description\", \"id\" : 6, \"type\" : \"HARDWARE\" }, { \"price\" : 1, \"name\" : \"name\", \"description\" : \"description\", \"id\" : 6, \"type\" : \"HARDWARE\" } ], \"status\" : \"REQUESTED\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * POST /orders : Request a order
     *
     * @param order Send the Order Object (required)
     * @return Null response (status code 201)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see OrdersApi#requestOrder
     */
    default ResponseEntity<Void> requestOrder(Order order) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /orders/{orderId} : Info for a specific order
     *
     * @param orderId The id of the order to retrieve (required)
     * @return Expected response to a valid request (status code 200)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see OrdersApi#showOrderById
     */
    default ResponseEntity<Order> showOrderById(String orderId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"reason\" : \"reason\", \"comment\" : \"comment\", \"id\" : 0, \"items\" : [ { \"price\" : 1, \"name\" : \"name\", \"description\" : \"description\", \"id\" : 6, \"type\" : \"HARDWARE\" }, { \"price\" : 1, \"name\" : \"name\", \"description\" : \"description\", \"id\" : 6, \"type\" : \"HARDWARE\" } ], \"status\" : \"REQUESTED\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
