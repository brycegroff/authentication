package family.groff.authentication.api;

import family.groff.authentication.interfaces.ItemsApi;
import family.groff.authentication.interfaces.ItemsApiDelegate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-10-08T14:53:29.739041194-07:00[America/Los_Angeles]")

@Controller
@RequestMapping("${openapi.simplySendProcurement.base-path:/v1}")
public class ItemsApiController implements ItemsApi {

    private final ItemsApiDelegate delegate;

    public ItemsApiController(@org.springframework.beans.factory.annotation.Autowired(required = false) ItemsApiDelegate delegate) {
        this.delegate = Optional.ofNullable(delegate).orElse(new ItemsApiDelegate() {});
    }

    @Override
    public ItemsApiDelegate getDelegate() {
        return delegate;
    }

}
