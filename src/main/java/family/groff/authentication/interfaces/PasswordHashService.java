package family.groff.authentication.interfaces;

public interface PasswordHashService {
    String hashPassword(String password);
}
