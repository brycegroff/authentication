package family.groff.authentication.interfaces;

import family.groff.authentication.api.ApiUtil;
import family.groff.authentication.api.UsersApiController;
import family.groff.authentication.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * A delegate to be called by the {@link UsersApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-10-08T14:53:29.739041194-07:00[America/Los_Angeles]")

public interface UsersApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /users : create a User
     *
     * @param user Send the User Object (required)
     * @return Null response (status code 201)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see UsersApi#createUser
     */
    default ResponseEntity<HashMap<String, List<String>>> createUser(User user) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /users : List all Users
     *
     * @param limit How many items to return at one time (max 100) (optional)
     * @return A paged array of users (status code 200)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see UsersApi#listUsers
     */
    default ResponseEntity<List<User>> listUsers(Integer limit) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"firstName\" : \"firstName\", \"lastName\" : \"lastName\", \"password\" : \"password\", \"role\" : \"MGR\", \"address\" : \"address\", \"phone\" : \"phone\", \"emailId\" : \"emailId\", \"id\" : 0, \"tag\" : \"tag\", \"managerName\" : \"managerName\", \"age\" : 6 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /users/{userId} : Info for a specific user
     *
     * @param userId The id of the user to retrieve (required)
     * @return Expected response to a valid request (status code 200)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see UsersApi#showUserById
     */
    default ResponseEntity<User> showUserById(String userId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"firstName\" : \"firstName\", \"lastName\" : \"lastName\", \"password\" : \"password\", \"role\" : \"MGR\", \"address\" : \"address\", \"phone\" : \"phone\", \"emailId\" : \"emailId\", \"id\" : 0, \"tag\" : \"tag\", \"managerName\" : \"managerName\", \"age\" : 6 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
