package family.groff.authentication.interfaces;

import family.groff.authentication.api.ApiUtil;
import family.groff.authentication.api.LoginApiController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

/**
 * A delegate to be called by the {@link LoginApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-10-08T14:53:29.739041194-07:00[America/Los_Angeles]")

public interface LoginApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /login : Login User
     *
     * @param emailId  (required)
     * @param password  (required)
     * @return Login response (status code 200)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see LoginApi#login
     */
    default ResponseEntity<String> login(String emailId,
                                         String password) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "false";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
