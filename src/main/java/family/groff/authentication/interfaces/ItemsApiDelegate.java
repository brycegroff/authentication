package family.groff.authentication.interfaces;

import family.groff.authentication.api.ApiUtil;
import family.groff.authentication.api.ItemsApiController;
import family.groff.authentication.models.Item;
import family.groff.authentication.models.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;

/**
 * A delegate to be called by the {@link ItemsApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-10-08T14:53:29.739041194-07:00[America/Los_Angeles]")

public interface ItemsApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /items : create an item (used by admin)
     *
     * @param item Send the Item Object (required)
     * @return Null response (status code 201)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see ItemsApi#createItem
     */
    default ResponseEntity<Void> createItem(Item item) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /items/{itemId} : Info for a specific item
     *
     * @param itemId The id of the item to retrieve (required)
     * @return Expected response to a valid request (status code 200)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see ItemsApi#getItemById
     */
    default ResponseEntity<Item> getItemById(String itemId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"price\" : 1, \"name\" : \"name\", \"description\" : \"description\", \"id\" : 6, \"type\" : \"HARDWARE\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /items : List all items available for purchase
     *
     * @param type type of the item HARDWARE,SOFTWARE,STATIONARY,TRAINING,MISC (optional)
     * @param limit How many items to return at one time (max 100) (optional)
     * @return A paged array of orders (status code 200)
     *         or Authentication information is missing or invalid (status code 401)
     *         or unexpected error (status code 200)
     * @see ItemsApi#listItems
     */
    default ResponseEntity<List<Order>> listItems(String type,
                                                  Integer limit) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"reason\" : \"reason\", \"comment\" : \"comment\", \"id\" : 0, \"items\" : [ { \"price\" : 1, \"name\" : \"name\", \"description\" : \"description\", \"id\" : 6, \"type\" : \"HARDWARE\" }, { \"price\" : 1, \"name\" : \"name\", \"description\" : \"description\", \"id\" : 6, \"type\" : \"HARDWARE\" } ], \"status\" : \"REQUESTED\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
