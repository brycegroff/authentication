package family.groff.authentication.api;

import family.groff.authentication.interfaces.UsersApi;
import family.groff.authentication.interfaces.UsersApiDelegate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-10-08T14:53:29.739041194-07:00[America/Los_Angeles]")

@Controller
@RequestMapping("${openapi.simplySendProcurement.base-path:/v1}")
public class UsersApiController implements UsersApi {

    private final UsersApiDelegate delegate;

    public UsersApiController(@org.springframework.beans.factory.annotation.Autowired(required = false) UsersApiDelegate delegate) {
        this.delegate = Optional.ofNullable(delegate).orElse(new UsersApiDelegate() {});
    }

    @Override
    public UsersApiDelegate getDelegate() {
        return delegate;
    }

}
